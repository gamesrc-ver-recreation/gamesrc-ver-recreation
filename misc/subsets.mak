SRCS=$(wildcard ENG_*.C)
OBJS=$(SRCS:.C=.OBJ)

$(info $(OBJS))

.PHONY: all

all: $(OBJS)

ENG_%.OBJ: ENG_%.C
	SDL_AUDIODRIVER=dummy dosbox -c "mount c ~/hdd/util/watcom" -c "c:" -c "call SPWATTST" -c "mount d ." -c "d:" -c "cycles=100000" -c "wcc386 eng_$* /4r /s /or -DLIBVER_BUILDREV=19960305" -c "wdisasm -l -e -p $@" -c "exit"
