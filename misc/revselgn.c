/* REVSEL.BAT generator. Gets input from stdin and writes output to stdout. */
#include <stdio.h>
#include <string.h>

enum {MAX_CHOICES = 36};
enum {CHOICE_BUFFER_LEN = 9};
enum {DESC_BUFFER_LEN = 76};

/* Attempts to read line with maximum length.
   Ending CR, LF or CR+LF chars, if present, are removed. */
static const char *read_line_from_stdin(char (*buffer)[80])
{
	const char *ret = fgets(*buffer, sizeof(*buffer), stdin);
	if (ret)
	{
		if ((*buffer)[0] && (*buffer)[strlen(*buffer)-1] == '\n')
			(*buffer)[strlen(*buffer)-1] = '\0';
		if ((*buffer)[0] && (*buffer)[strlen(*buffer)-1] == '\r')
			(*buffer)[strlen(*buffer)-1] = '\0';
	}
	return ret;
}

/* Pre-processes choices definitions file from stdin
   and writes generated batch script stdout.
   Input may use CR, LF or CR+LF chars for line endings,
   but the output is platform-dependent. */
int main(void)
{
	char buffer[80];
	char choiceStrs[MAX_CHOICES][CHOICE_BUFFER_LEN];
	char choiceDesc[MAX_CHOICES][DESC_BUFFER_LEN];
	int isDesc = 0, i, j, n = 0;
	int readExtraNotes = 0;
	/* Read a pair of strings at a time:
	   Short choice string, and descrption. */
	for (; read_line_from_stdin(&buffer); isDesc ^= 1)
	{
		if (n >= MAX_CHOICES)
		{
			fputs("Choices list overflow!", stderr);
			return 1;
		}

		if (!buffer[0])
		{
			readExtraNotes = 1;
			break;
		}

		if (isDesc)
		{
			strncpy(choiceDesc[n], buffer, sizeof(choiceDesc[n]));
			choiceDesc[n][DESC_BUFFER_LEN-1] = '\0';
			++n;
		}
		else
		{
			strncpy(choiceStrs[n], buffer, sizeof(choiceStrs[n]));
			choiceStrs[n][CHOICE_BUFFER_LEN-1] = '\0';
		}
	}

	/* Start writing to stdout */
	printf("@echo off\n");
	printf("if \"%%MSG%%\" == \"\" goto error\n");
	printf("set CHOICE=\n");
	printf("cls\n");

	/* Choices are numbered as follows: 0-9, then A-Z. */
	for (i = 0; i < n; ++i)
		printf("echo [%c] %s\n", i < 9 ? (i+'1') : (i-9+'A'), choiceDesc[i]);
	printf("echo.\n");
	printf("echo [0] Cancel and quit\n");
	printf("echo.\n");

	/* Print optional notes */
	if (readExtraNotes)
	{
		while (read_line_from_stdin(&buffer))
		{
			if (!buffer[0])
				break;
			printf("echo %s\n", buffer);
		}
		printf("echo.\n");
	}

	printf("echo %%MSG%%\n");
	printf("set MSG=\n");

	/* For choices identified by letters, lowercase
	   and uppercase inputs are both acceptable. */
	printf("choice /S /C:");
	for (i = 0; i < n; ++i)
		if (i < 9)
			printf("%c", i+'1');
		else
			printf("%c%c", i-9+'A', i-9+'a');
	printf("0 /N\n");
	printf("echo.\n");
	printf("\n");

	/* Print list of gotos from the last to the first. Note that
	   any choice identified by a letter matches two error levels. */
	j = (n <= 9) ? (n + 1) : 2 * (n - 9) + 10;
	printf("if ERRORLEVEL %d goto end\n", j);
	for (i = n - 1, j -= (j > 10) ? 2 : 1; i >= 0; --i, j -= (j > 10) ? 2 : 1)
		printf("if ERRORLEVEL %d goto %s\n", j, choiceStrs[i]);
	printf("\n");

	for (i = 0; i < n; ++i)
	{
		printf(":%s\n", choiceStrs[i]);
		printf("set CHOICE=%s\n", choiceStrs[i]);
		printf("goto end\n");
	}

	printf("\n");
	printf(":error\n");
	printf("echo This script shouldn't be run independently\n");
	printf("\n");
	printf(":end\n");

	return 0;
}
