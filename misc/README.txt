This directory includes two helper scripts, which may be run with GNU Awk
i.e., gawk. Any other Awk variant might be *incompatible*!

It also has revselgn.c, used for generating REVSEL.BAT from REVSEL.DEF files.

Each of the awk scripts should get an output LST file from IDA Free v4.1.
The file has to be passed as an argument *twice* (since it's processed twice).

Short explanations of the scripts and program:  
- print_replaced_var_suffixes_mappings_from_asm_lst.awk: Goes over textual
mentions of global variables and maps them to alternative names, based
on the address in the binary code. The file is scanned *twice* so the
*last* relevant occurrence of the variable name counts.
- replace_var_suffixes_in_asm_lst.awk: This one is similar, but prints
the LST file after renaming the global variables (during the second scan).
- revselgn.c reads from stdin the contents of an input REVSEL.DEF file,
which may list differing versions of an application or library. It writes
to stdout the contents of a REVSEL.BAT file letting the user select such
a version. Each version is represented in REVSEL.DEF by two consecutive
lines, with the first being a short string, and the second being a textual
description. Optionally, this list may be followed by an empty line,
which is then followed by a comment that is echoed in the BAT file.
While the input DEF file may use different new line formats (CR, LF or CR+LF),
the output's format depends on the platform for which revselgn.c is compiled.
You might have to go through a process to convert LF newlines to CR+LF.
Limitations in the input contents related to uses in BAT files may apply.
For instance, the short identifier strings shouldn't contain space characters.

Regarding the awk scripts, note that they are intended for use ONLY
with .lst files generated from 32-bit DOS code built with Watcom C
(versions 9.5-11.0c should be covered).

FURTHER BE WARNED THAT ANY INCLUDED SCRIPT OR PROGRAM
MAY FAIL FOR ANY ARBITRARY REASON.
