gamesrc-ver-recreation hosts modifications of old programs' released sources,
the purpose being the reproduction of originally released EXEs, byte-by-byte
(or at least of EXEs close to these). A few related libraries are also covered.

It currently has modified sources used for reproducing the following,
with the extent of accuracy varying across code bases and versions of them:

- One recreated version of Catacomb 3-D, and one of The Catacomb Abyss.
- Source reconstructions for LOADSCN.EXE as bundled
with The Catacomb Abyss v1.13, plus INTRO.EXE and
DEMOCAT.EXE/HINTCAT.EXE from the Catacomb Adventure Series.
- Multiple builds of Wolfenstein 3D and Spear of Destiny, as well
as recreated code for the DOS port of Super 3-D Noah's Ark (v1.0).
- Various builds of Blake Stone: Aliens of Gold and Planet Strike.
- Various builds of Rise of the Triad identified as v1.3.
- The non-DMX code in multiple DOS versions of Doom, along with Chex Quest.
- The non-DMX code in multiple DOS versions of Heretic.
- The non-DMX code in multiple DOS versions of Hexen.
- The non-DMX code in multiple DOS versions of Strife.
- The two known DOS versions of Duke Nukem 3D: Atomic Edition.
Also covered are NAM (NAPALM), WW2GI and Enhanced Duke (EDuke) 2.00.23.
- Two versions of the Duke3D source addendum for the Build Editor
(ASTUB.C code only), used for the editor as distributed with Duke Nukem 3D.

Recreations of the the following libraries are additionally covered,
again with varying levels of accuracy:

- A few versions of the Apogee Sound System.
- The Build Engine's object files (also includes the Ken-Build test game).

Also covered are the following:
- APODMX or The Apogee Sound System backed DMX wrapper, a DMX sound library
wrapper which actually uses the Apogee Sound System.
- Miscellaneous files under the directory named "misc".

Contents added in the past, but generally not updated or used anymore:
- Descriptions of specific source code changes, as found in the "patches"
subdirectory. Diffs to some files (like binary .OBJ files) might be absent.
- The "stripbss" program, which was used in the past for the reproduction
of at least a few executables, but was later found to not be needed.
Chances are a Borland C++ project setting could be used, possibly
a toggle of debug info, in conjunction with packing the exe.

Thanks should be sent to members of a few communities at the least, for help
and other kinds of support; Anybody making the releases of these source codes
possible, the original developers for the mere existence of the games and
related programs or libraries, and more individuals.
